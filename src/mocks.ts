import { Users, Routes } from "./types";

let usersMock: Users = {
  111: {
    name: "Jonny",
    phones: [{ number: "+380991234567", isValid: true, dateRegistered: "abc" }],
    dateRegistered: "abc",
  },
  222: {
    name: "Ashot",
    phones: [{ number: "+380999876543", isValid: true, dateRegistered: "deb" }],
    dateRegistered: "deb",
  },
};
let routesMock: Routes = {
  57093431: {
    userId: "111",
    role: 1,
    points: {
      from: ["p1"],
      to: ["p2", "p3"],
    },
    dateTravel: "Sat, 30 Jul 2022 19:00:56 GMT",
    dateRegistered: "Sat, 30 Jul 2022 19:00:56 GMT",
  },
  74014489: {
    userId: "111",
    role: 1,
    points: {
      from: ["p1"],
      to: ["p2", "p3"],
    },
    dateTravel: "Sat, 30 Jul 2022 20:00:56 GMT",
    dateRegistered: "Sat, 30 Jul 2022 20:00:56 GMT",
  },
  90781432: {
    userId: "222",
    role: 1,
    points: {
      from: ["p4"],
      to: ["p2", "p3"],
    },
    dateTravel: "Sat, 30 Jul 2022 20:00:56 GMT",
    dateRegistered: "Sat, 30 Jul 2022 20:00:56 GMT",
  },
  85648902: {
    userId: "222",
    role: 2,
    points: {
      from: ["p1"],
      to: ["p2"],
    },
    dateTravel: "Sat, 30 Jul 2022 19:10:56 GMT",
    dateRegistered: "Sat, 30 Jul 2022 19:10:56 GMT",
  },
  78004905: {
    userId: "222",
    role: 2,
    points: {
      from: ["p1"],
      to: ["p2"],
    },
    dateTravel: "Sat, 30 Jul 2022 18:46:56 GMT",
    dateRegistered: "Sat, 30 Jul 2022 18:46:56 GMT",
  },
};
