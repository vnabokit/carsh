import express, { Request, Response } from "express";
import { Users, Routes, RouteInfo, UserInfo } from "./types";
import { saveRoute, saveUser, getRouteById, findRoutes } from "./functions";

const PORT = 3000;

let users: Users = {};
let routes: Routes = {};

let app = express();
app.use(express.json());

/*
{
  "userId" : "qwerty123",
  "role" : 1,
  "points" : {
    "from": ["p1"],
    "to": ["p2", "p3"]
  } ,
  "dateTravel" : "Sat, 30 Jul 2022 18:46:56 GMT"
}
*/
app.post("/routes/add", (req: Request, resp: Response) => {
  const { userId, role, points, dateTravel } = req.body;
  if (!userId || !role || !points || !dateTravel) {
    resp.status(400).send({ message: "Not enough data" });
  }
  const route: RouteInfo = {
    userId,
    role,
    points,
    dateTravel,
    dateRegistered: new Date().toUTCString(),
  };
  const routeId = saveRoute(route, routes);
  resp.status(200).send({ message: "Route saved", payload: { routeId } });
  console.log("routes", routes);
});

/*
{
  "name" : "Jonny",
  "phoneNumber" : "+380991234567"
}
*/
app.post("/users/register", (req: Request, resp: Response) => {
  const { name, phoneNumber } = req.body;
  if (!name || !phoneNumber) {
    resp.status(400).send({ message: "Not enough data" });
  }
  const dateRegistered = new Date().toUTCString();
  const user: UserInfo = {
    name,
    dateRegistered,
    phones: [{ number: phoneNumber, dateRegistered, isValid: true }],
  };
  const userId = saveUser(user, users);
  resp.status(200).send({ message: "User saved", payload: { userId } });
  console.log("users", users);
});

app.get("/routes/find/:routeid", (req: Request, resp: Response) => {
  const routeId = req.params.routeid;
  const route = getRouteById(routeId, routes);
  if (!route) {
    resp.status(400).send({ message: "Router not found" });
    return;
  }
  const foundRoutes = findRoutes(route, routes);
  if (foundRoutes.length == 0) {
    resp.status(404).send({ message: "Routers not found" });
    return;
  }
  resp.status(200).send(foundRoutes);
});

app.listen(PORT, () => {
  console.log(`App listens on port ${PORT}`);
});
