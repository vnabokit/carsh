import { Users, Routes, RouteInfo, UserInfo } from "./types";

export const generateId = (): string => {
  return Math.random().toString().substr(2, 8);
};

export const saveRoute = (route: RouteInfo, routes: Routes): string => {
  const routeId = generateId();
  //   Object.assign(routes, { routeId, route });
  routes[routeId] = route;
  return routeId;
};

export const saveUser = (user: UserInfo, users: Users): string => {
  const userId = generateId();
  //   Object.assign(users, { userId, user });
  users[userId] = user;
  return userId;
};

export const isTimeFit = (route1: RouteInfo, route2: RouteInfo): boolean => {
  const ratioSeconds = 1800;
  return (
    Math.abs(
      new Date(route1.dateTravel).getTime() / 1000 -
        new Date(route2.dateTravel).getTime() / 1000
    ) <= ratioSeconds
  );
};

export const isRoleFit = (route1: RouteInfo, route2: RouteInfo): boolean => {
  return route1.role != route2.role;
};

export const isPointsFit = (route1: RouteInfo, route2: RouteInfo): boolean => {
  const fromIntersection = route1.points.from.filter((x: string) =>
    route2.points.from.includes(x)
  );
  const toIntersection = route1.points.to.filter((x: string) =>
    route2.points.to.includes(x)
  );
  return fromIntersection.length > 0 && toIntersection.length > 0;
};

export const findRoutes = (route0: RouteInfo, routes: Routes): RouteInfo[] => {
  let selectedRoutes: RouteInfo[] = [];
  for (const [routeId, routeI] of Object.entries(routes)) {
    if (routeI.userId == route0.userId) {
      continue;
    }
    if (!isTimeFit(routeI, route0)) {
      continue;
    }
    if (!isRoleFit(routeI, route0)) {
      continue;
    }
    if (!isPointsFit(routeI, route0)) {
      continue;
    }
    selectedRoutes.push(routeI);
  }
  return selectedRoutes;
};

export const getRouteById = (
  routeId0: string,
  routes: Routes
): RouteInfo | null => {
  let foundRoute = null;
  for (let routeId in routes) {
    if (routes.hasOwnProperty(routeId)) {
      if (routeId0 === routeId) {
        foundRoute = routes[routeId];
        break;
      }
    }
  }
  return foundRoute;
};
