export type RouteInfo = {
  userId: string;
  role: 1 | 2;
  points: { from: string[]; to: string[] };
  dateTravel: string;
  dateRegistered: string;
};

export type UserPhone = {
  number: string;
  isValid: boolean;
  dateRegistered: string;
};

export type UserInfo = {
  name: string;
  phones: UserPhone[];
  dateRegistered: string;
};

export type Users = {
  [key: string]: UserInfo;
};

export type Routes = {
  [key: string]: RouteInfo;
};

export type ResponseFormat = {
  message: string;
  payload?: any;
};
